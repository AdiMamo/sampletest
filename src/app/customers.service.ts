import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  
  updateResult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        result:result
      })
    }

  public getCustomers(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`); 
    return this.customerCollection.snapshotChanges();
} 


  updateCustomer(userId:string,id:string, name:string, education:number, income:number){
      this.db.doc(`users/${userId}/customers/${id}`).update(
        {
          name:name,
          education:education,
          income:income,
          result:null
        }
      )
    }


deteleCustomer(userId:string ,id:string){
  this.db.doc(`users/${userId}/customers/${id}`).delete();
}

addCustomer(userId:string,name:string,education:number,income:number){
  const customer = {name:name,education:education,income:income}; 
  this.userCollection.doc(userId).collection('customers').add(customer);
}

  constructor(private db:AngularFirestore) { }
}
