import { PredictionService } from './../prediction.service';
import { Customer } from './../interfaces/customer';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { NumberValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  displayedColumns: string[] = ['name', 'education', 'income', 'delete','edit','predict','result'];

  customers$;
  userId:string;
  customers:Customer[];
  addCustomerFormOpen = false;
  rowToEdit:number = -1; 
  customerToEdit:Customer = {id:null,name:null, education:null, income:null};

  constructor(private customerService:CustomersService, public authService:AuthService, public predictionService:PredictionService) { }

    // update(customer:Customer){
    //     this.customerService.updateCustomer(this.userId,customer.id, customer.name, customer.education, customer.income);
    //   }

    updateResult(index){
      this.customers[index].saved = true; 
      this.customerService.updateResult(this.userId,this.customers[index].id,this.customers[index].result);
    }
      
    moveToEditState(index){
      console.log(this.customers[index].name);
      this.customerToEdit.name = this.customers[index].name;
      this.customerToEdit.education = this.customers[index].education;
      this.customerToEdit.income = this.customers[index].income;
      this.rowToEdit = index; 
    }
    updateCustomer(){
          let id = this.customers[this.rowToEdit].id;
          this.customerService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.education,this.customerToEdit.income);
          this.rowToEdit = null;

  }

  deleteCustomer(id:string){
    this.customerService.deteleCustomer(this.userId,id);
  }

  add(customer:Customer){
    this.customerService.addCustomer(this.userId,customer.name,customer.education,customer.income); 
  }

  predict(index){
    this.predictionService.predict(this.customers[index].education, this.customers[index].income).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will pay';
          console.log(result)
        } else {
          var result = 'Will not pay'
          console.log(result)
        }
      this.customers[index].result = result}
    );  
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.customers$ = this.customerService.getCustomers(this.userId);
        
        this.customers$.subscribe(
          docs =>{
            this.customers = [];
            for(let document of docs){
              const customer:Customer = document.payload.doc.data();
              if(customer.result){
                customer.saved = true; 
              }
              customer.id = document.payload.doc.id; 
              this.customers.push(customer); 
            }
          }
        ) 


        }
      )
  }
}
