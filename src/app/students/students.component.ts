import { AuthService } from './../auth.service';
import { StudentsService } from './../students.service';
import { LambdaService } from './../lambda.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  math:number;
  psycho:number;
  options:Object[] = [{id:1,name:'Yes'},{id:2,name:'No'}]
  option:string;
  pay;
  result:string;
  email:string;

  cancel(){
    this.result = null;
  }

  add(){
    this.studentsService.addStudent(this.math, this.psycho, this.pay, this.result, this.email); 
    this.router.navigate(['/table'])

  }

  predict(){
    this.lambdaService.predict(this.math,this.psycho,this.pay).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will Dropout';
          console.log(result)
        } else {
          var result = 'Will not Dropout'
          console.log(result)
        }
      this.result = result}
    );  
  }


  constructor(public lambdaService:LambdaService,private studentsService:StudentsService, public authService:AuthService,private router:Router) { }

  ngOnInit(): void {

   this.authService.getUser().subscribe(
     user => {
     this.email = user.email;   
          }
                )
  }
}