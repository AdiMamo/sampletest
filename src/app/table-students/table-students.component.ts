import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../students.service';

@Component({
  selector: 'app-table-students',
  templateUrl: './table-students.component.html',
  styleUrls: ['./table-students.component.css']
})
export class TableStudentsComponent implements OnInit {

  displayedColumns: string[] = ['math', 'psycho', 'pay', 'email','result','delete'];
  students$
  students = [];

  delete(id:string){
    this.studentsService.deteleStudent(id);
  }


  constructor(private studentsService:StudentsService) { }

  ngOnInit(): void {
    this.students$ = this.studentsService.getStudents();
        
    this.students$.subscribe(
      docs =>{
        this.students = [];
        for(let document of docs){
          const students = document.payload.doc.data();
          students.id = document.payload.doc.id; 
          this.students.push(students); 
        }
      }
    ) 

    }
}
