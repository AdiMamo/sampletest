import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LambdaService {

  private url = "https://gpttgvqg88.execute-api.us-east-1.amazonaws.com/beta";

  predict(math:number, psycho:number, pay:boolean){
    let json = {"data":
      {"math":math,
       "psycho":psycho,
       "pay":pay}
    }

    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        return res.body; 
        
      })
    );      
  }

  constructor(private http:HttpClient) { }
}

