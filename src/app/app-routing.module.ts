import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { CustomersComponent } from './customers/customers.component';
import { PostsComponent } from './posts/posts.component';
import { SavedPostsComponent } from './saved-posts/saved-posts.component';
import { StudentsComponent } from './students/students.component';
import { TableStudentsComponent } from './table-students/table-students.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'customers', component: CustomersComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'myPost', component: SavedPostsComponent },
  { path: 'students', component: StudentsComponent },
  { path: 'table', component: TableStudentsComponent }




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 