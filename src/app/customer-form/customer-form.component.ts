import { Customer } from './../interfaces/customer';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  isError:boolean = false;
  @Input() id:string;
  @Input() name:string;  
  @Input() education:number; 
  @Input() income:number; 
  @Input() formType:string;
  @Output() update = new EventEmitter<Customer>();
  @Output() closeForm = new EventEmitter<null>();
  
  updateParent(){
    let customers:Customer = {id:this.id, name:this.name, education:this.education, income:this.income};
    if(this.education<0 || this.education>24){
      this.isError = true;
    }else{
    this.update.emit(customers);
    if(this.formType == "Add Customer"){
      this.name  = null;
      this.education = null;
      this.income = null; 
    }
  }
}

  tellParentToClose(){
    this.closeForm.emit(); 
  }

  constructor() { }

  ngOnInit(): void {


  }
}
