import { Post } from './../interfaces/post';
import { PostsService } from './../posts.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {

  posts$:Observable<any>;
  userId:string;
  constructor(public auth:AuthService, private postService:PostsService) { }

  Like(id:string, numLike:number){
    if(isNaN(numLike)){
      numLike = 1; 
    }else{
      numLike ++
    }
    this.postService.updateLike(this.userId,id,numLike)
  }

  deletePost(id:string){
    this.postService.deletePost(this.userId,id);
  }

  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.posts$ = this.postService.getPost(this.userId);        
  }
    ) 
  }
}