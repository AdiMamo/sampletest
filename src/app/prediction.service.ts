import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
private url = "https://970mdjum84.execute-api.us-east-1.amazonaws.com/beta";

  predict(education:number, income:number){
    let json = {"data":
      {"education":education,
       "income":income}
    }

    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        return res.body; 
        
      })
    );      
  }

  constructor(private http:HttpClient) { }
}
