import { Comment } from './../interfaces/comment';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Post } from '../interfaces/post';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  
  posts$:Observable<Post>;
  comments$:Observable<Comment>;
  userId:string;
  isShown:boolean = false ; // hidden by default
  saveState = [];
  constructor(private postService:PostsService,public authService:AuthService) { }

  savePost(id:number,title:string,body:string){
    this.postService.addPost(this.userId,id,title,body);
    this.isShown = true;
}

  ngOnInit(): void {
    this.posts$ =  this.postService.getPosts();
    this.comments$ =  this.postService.getComments();
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
      }
    )
  }
}
