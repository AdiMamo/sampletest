import { Comment } from './interfaces/comment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from './interfaces/post';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private URL = "https://jsonplaceholder.typicode.com/posts/";
  private URLcomment = "https://jsonplaceholder.typicode.com/comments/";
  
  postsCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  updateLike(userId:string, id:string, numLike:number){
    this.db.doc(`users/${userId}/posts/${id}`).update(
      {
        numLike:numLike
      }
    )
  }

  deletePost(Userid:string, id:string){
    this.db.doc(`users/${Userid}/posts/${id}`).delete(); 
  }

  addPost(userId:string,id:number,title:string,body:string){
    const posts = {id:id,title:title,body:body};
    this.userCollection.doc(userId).collection('posts').add(posts);
     }

    getPost(userId){
      this.postsCollection = this.db.collection(`users/${userId}/posts`);
      return this.postsCollection.snapshotChanges().pipe(map(
        collection =>collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data;
                }
             )
            ))
          }

  getPosts():Observable<Post>{
    return this.http.get<Post>(this.URL);
  }

  getComments():Observable<Comment>{
    return this.http.get<Comment>(this.URLcomment);
  }

  constructor(private http:HttpClient,private db:AngularFirestore) { }
}
