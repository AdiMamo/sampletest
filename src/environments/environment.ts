// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDQbarc3usYWKcGeIjs0Mdy3VDLwIEorz0",
    authDomain: "sampletest-a28ae.firebaseapp.com",
    projectId: "sampletest-a28ae",
    storageBucket: "sampletest-a28ae.appspot.com",
    messagingSenderId: "909131139944",
    appId: "1:909131139944:web:16041994fd073a630c24dd"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
